import { Cw2Page } from './app.po';

describe('cw2 App', () => {
  let page: Cw2Page;

  beforeEach(() => {
    page = new Cw2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
