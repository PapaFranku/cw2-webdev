<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if(!array_key_exists("u", $_REQUEST) || !array_key_exists("m", $_REQUEST))
{
    exit();
}

$num = strip_tags($_REQUEST["u"]);
$mod = strip_tags($_REQUEST["m"]);

$connection = new mysqli("localhost", "40211765", "HUKzERH3", "40211765");

if($connection->connect_error)
{
    exit();
}

$sql = "SELECT INS_RES.QUE_CODE,CAT_CODE,QUE_NAME,QUE_TEXT, RES_VALU
        FROM INS_RES
        JOIN INS_QUE ON INS_RES.QUE_CODE = INS_QUE.QUE_CODE
        WHERE INS_RES.SPR_CODE='{$num}' AND
              INS_RES.MOD_CODE='{$mod}'";

$statement = $connection->query($sql);

$data = array();

#Check if the student has answered the questions
if($connection->affected_rows != 0)
{
    while($row = $statement->fetch_row())
    {
        $data[] = $row;
    }
}
else #If the student hasn't answered any questions then return the questions with rating of 0;
{
    $sql="SELECT * from INS_QUE";

    $statement = $connection->query($sql);

    while($row = $statement->fetch_row())
    {
        $row[] = "0";
        $data[] = $row;
    }
}

print json_encode($data);
?>