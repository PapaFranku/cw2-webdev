<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if(!array_key_exists("m", $_REQUEST))
{
    exit();
}

$num = strip_tags($_REQUEST["m"]);

$connection = new mysqli("localhost", "40211765", "HUKzERH3", "40211765");

if($connection->connect_error)
{
    exit();
}

$sql = "SELECT INS_RES.MOD_CODE, INS_RES.QUE_CODE, QUE_TEXT,CAT_NAME,
        ROUND(100*SUM(FLOOR(RES_VALU/4))/COUNT(1)) as score
        FROM INS_RES JOIN INS_QUE ON INS_RES.QUE_CODE=INS_QUE.QUE_CODE
                     JOIN INS_CAT ON INS_QUE.CAT_CODE=INS_CAT.CAT_CODE
        WHERE INS_RES.MOD_CODE='{$num}'
        AND INS_RES.AYR_CODE='2016/7'
        AND INS_RES.PSL_CODE='TR1'
        GROUP BY MOD_CODE, QUE_CODE,QUE_TEXT,CAT_NAME";

$statement = $connection->query($sql);
$data = array();
while($row = $statement->fetch_row())
{
    $data[] = $row;
}

print json_encode($data);
?>