<?php
header('Access-Control-Allow-Origin: *');

if(!array_key_exists("u", $_REQUEST) || !array_key_exists("m", $_REQUEST))
{
    exit();
}

$num = strip_tags($_REQUEST["u"]);
$mod = strip_tags($_REQUEST["m"]);

$connection = new mysqli("localhost", "40211765", "HUKzERH3", "40211765");

if($connection->connect_error)
{
    exit();
}

$sql = "SELECT INS_RES.QUE_CODE,CAT_CODE,QUE_NAME,QUE_TEXT, RES_VALU
        FROM INS_RES
        JOIN INS_QUE ON INS_RES.QUE_CODE = INS_QUE.QUE_CODE
        WHERE INS_RES.SPR_CODE='{$num}' AND
              INS_RES.MOD_CODE='{$mod}'";

$statement = $connection->query($sql);

if($connection->affected_rows != 0)
{
    $sql = "UPDATE INS_RES 
            SET RES_VALU=?
            WHERE SPR_CODE='{$num}' AND
                  MOD_CODE='{$mod}' AND
                  QUE_CODE=?";
}
else
{
    $sql = "INSERT INTO INS_RES VALUES ({$num},{$mod},'2016/7','TR1',?,?)";
}

$statement = $connection->prepare($sql) or die();
$statement->bind_param("id", $rating, $code);

foreach($_REQUEST as $key => $value)
{
    if($key == "u" || $key == "m")
    {
        continue;
    }
    
    $key = str_replace("_", ".", $key);
        
    $rating = $value;
    $code = $key;

    $statement->execute() or die();
}
?>