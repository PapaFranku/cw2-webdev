import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { Question } from "../components/module/IQuestion";

@Injectable()
export class UniService //Provides an interface for interacting with the database
{
  private baseURL: string = "http://40211765.set08101.napier.ac.uk/cw2/php/";

  constructor(private http: Http) 
  { 

  }

  getModulesForStudent(studentMatric: string)
  {
    var url = this.baseURL + "retrieve-student.php?u=" + studentMatric;

    return this.http.get(url).map(res => res.json());
  }

  getQuestions(studentMatric: string, moduleCode: string)
  {
    var url = this.baseURL + "retrieve-questions.php?u=" +
      studentMatric + "&m=" + moduleCode;

    return this.http.get(url).map(res => res.json());
  }

  getModuleStatistics(moduleCode: string)
  {
    var url = this.baseURL + "retrieve-statistics.php?m=" + moduleCode;

    return this.http.get(url).map(res => res.json());
  }

  postAnswers(studentMatric: string, moduleCode: string, questions: Question[])
  {
    var url = this.baseURL + "update-answers.php?";

    var body = "u=" + studentMatric + "&m=" + moduleCode;

    //Construct the url
    questions.forEach(element =>
    {
      body += "&" + element.id + "=" + element.rating;
    })

    return this.http.post(url+body, "");
  }
}
