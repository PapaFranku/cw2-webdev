import { TestBed, inject } from '@angular/core/testing';

import { UniService } from './uni.service';

describe('UniService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UniService]
    });
  });

  it('should ...', inject([UniService], (service: UniService) => {
    expect(service).toBeTruthy();
  }));
});
