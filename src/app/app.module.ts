import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { StudentHomeComponent } from './components/student-home/student-home.component';
import { routes } from "./routes";
import { UniService } from "./services/uni.service";
import { ModuleComponent } from './components/module/module.component';
import { RatingModule } from 'ng2-bootstrap/rating';
import { RatingComponent } from './components/rating/rating.component';
import { ProgressbarModule } from 'ng2-bootstrap/progressbar';
import { CollapseModule } from 'ng2-bootstrap/collapse';
import { StaffHomeComponent } from './components/staff-home/staff-home.component';
import { ScoresTableComponent } from './components/scores-table/scores-table.component';
import { ChartsModule } from 'ng2-charts';
import { DatachartComponent } from './components/datachart/datachart.component';
import { ChartdataPipe } from "./pipes/chartdata/chartdata.pipe";
import { ChartlabelsPipe } from './pipes/chartlabels/chartlabels.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    StudentHomeComponent,
    ModuleComponent,
    RatingComponent,
    StaffHomeComponent,
    ScoresTableComponent,
    DatachartComponent,
    ChartdataPipe,
    ChartlabelsPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routes,
    RatingModule.forRoot(),
    ProgressbarModule.forRoot(),
    CollapseModule.forRoot(),
    ChartsModule
  ],
  providers: [ UniService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
