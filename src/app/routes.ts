import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";

import { AppComponent } from "./app.component";
import { StudentHomeComponent } from "./components/student-home/student-home.component";
import { ModuleComponent } from "./components/module/module.component";
import { StaffHomeComponent } from "./components/staff-home/staff-home.component";

//Represents the possible routes for the application
export const routes: ModuleWithProviders = RouterModule.forRoot(
[
    {
        path: "",
        component: StudentHomeComponent
    },
    {
        path: "output",
        component: StaffHomeComponent
    },
    {
        path: "output.html",
        redirectTo: "output"
    }
]);