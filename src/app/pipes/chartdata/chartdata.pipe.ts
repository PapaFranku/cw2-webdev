import { Pipe, PipeTransform } from '@angular/core';

import { DataRow } from "../../components/scores-table/IDataRow";
import { ChartData } from "../../components/datachart/IChartdata";

@Pipe({
  name: 'chartdata'
})
export class ChartdataPipe implements PipeTransform //Used to filter module data,
{                                                   //in suitable format for the datachart component
  transform(moduleData1: DataRow[], moduleData2: DataRow[]): ChartData[]
  {
    var dataArray: ChartData[] = [];
    var dataObject: ChartData =
    {
      data: [],
      label: moduleData1[0].moduleCode
    }

    moduleData1.forEach(element => 
    {
      dataObject.data.push(parseInt(element.agreeRate));
    });

    dataArray.push(dataObject);

    if(moduleData2)
    {
      var dataObject: ChartData =
      {
        data: [],
        label: moduleData2[0].moduleCode
      }

      moduleData2.forEach(element => 
      {
        dataObject.data.push(parseInt(element.agreeRate));
      });

      dataArray.push(dataObject);
    }

    return dataArray;
  }
}
