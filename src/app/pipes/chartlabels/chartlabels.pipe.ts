import { Pipe, PipeTransform } from '@angular/core';

import { DataRow } from "../../components/scores-table/IDataRow";

@Pipe({
  name: 'chartlabels'
})
export class ChartlabelsPipe implements PipeTransform //Used to filter the question numbers for the
{                                                     //datachart component
  transform(value: DataRow[], args?: any): string[] 
  {
    var labels: string[] = [];

    value.forEach(element => 
    {
      labels.push(element.questionNumber);
    });

    return labels;
  }
}
