import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationExtras } from "@angular/router";

import { UniService } from "../../services/uni.service";
import { DataRow } from "../scores-table/IDataRow";

@Component({
  selector: 'staff-home',
  templateUrl: './staff-home.component.html',
  styleUrls: ['./staff-home.component.css']
})
export class StaffHomeComponent //Reperesents the container for the output page
{
  comparisonMode: boolean; //True if comparing two modules
  module1Data: DataRow[]; //Holds data for the first module to compare
  module2Data: DataRow[]; //Holds data for the second module to compare

  constructor(private activatedRoute: ActivatedRoute,
              private uniService: UniService)
  { }

  private assignRows(dataArray): DataRow[] //Arrange the data received
  {
    if(dataArray.length == 0) //If no data, return
    {
      return;
    }

    var data: DataRow[] = [];

    for(var i in dataArray)
    {
      var row: DataRow =
      {
        moduleCode: dataArray[i][0],
        section: dataArray[i][3],
        questionNumber: dataArray[i][1],
        question: dataArray[i][2],
        agreeRate: dataArray[i][4]
      }

      data.push(row);
    }

    return data;
  }

  onSubmit(module1: string, module2?: string) //Called when the on of the two submit buttons is pressed
  {                                             //The parameters are the text box values
    this.uniService.getModuleStatistics(module1)
      .subscribe(res =>
      { 
        this.module1Data = this.assignRows(res);
      });
    
    if(module2) //If there was a second parameter passed
    {
      this.uniService.getModuleStatistics(module2)
        .subscribe(res => 
        {
          this.module2Data = this.assignRows(res);
        });
      
      this.comparisonMode = true;
    }
    else
    {
      this.comparisonMode = false;
    }
  }
}
