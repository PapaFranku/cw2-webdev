import { Component, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'rating-bar',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent //Represents the rating bar in the UI
{
  currentRating: number; //Holds the current rating
  @Output() ratingEmitter: EventEmitter<number>; //Emits a new value to the parent component when the rating has changed
  overStar:number; //Represents which circle is being hovered on
  text: string; //Represents the text in the label

  constructor()
  {
    this.ratingEmitter = new EventEmitter<number>();
  }

  ratingChanged(rating: number) //Called when the user has clicked on a circle
  {
    this.ratingEmitter.emit(rating); //Emit the new rating to the parent component
  }
 
  hoveringOver(value: number)
  {
    this.overStar = value;

    //Change the text depending on the circle which is hovered 
    switch(value)
    {
      case 1:
        this.text = "Definitely Disagree";
        break;
      case 2:
        this.text = "Mostly Disagree";
        break;
      case 3:
        this.text = "Neither Agree nor Disagree";
        break;
      case 4:
        this.text = "Mostly Agree";
        break;
      case 5:
        this.text = "Definitely Agree";
        break;
      default:
        break;
    }
  };

  changeRating(rating: number) //Called by the parent component to change the current rating
  {
      this.currentRating = rating;
  }
}
