import { Component } from '@angular/core';

@Component(
{
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent //Represents the navbar
{
  isCollapsed: boolean = true;
}
