import { Component, Input } from '@angular/core';

import { ChartData } from "./IChartdata";

@Component({
  selector: 'datachart',
  templateUrl: './datachart.component.html',
  styleUrls: ['./datachart.component.css']
})
export class DatachartComponent //Represents a single chart
{
  @Input() moduleLabels: string[]; //Input data is received from the parent component
  @Input() data: ChartData[];
}
