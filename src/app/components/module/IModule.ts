export interface Module
{
  moduleCode: string,
  moduleName: string
}