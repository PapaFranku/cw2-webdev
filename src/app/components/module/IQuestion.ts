export interface Question
{
    id: number,
    categoryCode: number,
    name: string,
    content: string,
    rating: number
}