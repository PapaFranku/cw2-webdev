import { Component, Input, OnInit, ViewChild, AfterContentChecked } from '@angular/core';

import { UniService } from "../../services/uni.service";
import { Module } from "./IModule";
import { Question } from "./IQuestion";
import { RatingComponent } from "../rating/rating.component";

@Component({
  selector: 'module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css']
})

export class ModuleComponent implements OnInit, AfterContentChecked //Represents a single module
{
  @Input() module: Module;         //Input data is received from the parent component
  @Input() studentMatric: string;  
  questions: Question[];          //Holds the questions and their rating for the module
  showFooter: boolean = false;
  currentQuestion: Question;      //Represents the current question in the UI
  currentQuestionIndex: number;
  ratingBarPercentage: number;    //Used by the progress bar  
  questionsRated: number;         //Holds the total count of the questions which were rated
  @ViewChild("ratingBar") ratingBar: RatingComponent; //Holds a reference to the RatingComponent if rendered

  constructor(private uniService: UniService) {  }

  ngOnInit() //Called when the component is instantiated
  {
    //Get the questions from the service
    this.uniService.getQuestions(this.studentMatric, this.module.moduleCode)
      .subscribe(res => 
    {
      this.questions = this.assignQuestions(res);
      this.currentQuestion = this.questions[0];
      this.ratingBarPercentage = Math.round(100 * (this.questionsRated / this.questions.length));
    });

    //Initialize the variables
    this.ratingBarPercentage = 0;
    this.questionsRated = 0;
    this.currentQuestionIndex = 1;
  }

  ngAfterContentChecked() //Used to update the UI with the current rating, if the panel body was hidden
  {
    if(this.ratingBar)
    {
      this.ratingBar.changeRating(this.currentQuestion.rating);
    }
  }

  onRatingChanged(rating: number) //Called when the rating has changed
  {
    if(this.currentQuestion.rating == 0)
    {
      this.questionsRated++;
      this.ratingBarPercentage = Math.round(100 * (this.questionsRated / this.questions.length));
    }

    this.currentQuestion.rating = rating;
  }

  onSubmit() //Called when the submit button was pressed
  {
    if(this.ratingBarPercentage == 100)
    {
      this.uniService.postAnswers(this.studentMatric, 
        this.module.moduleCode, this.questions)
        .subscribe(res =>
        {
          if(res.status == 200)
          {
            alert("Success!");
          }
          else
          {
            alert("Something went wrong...");
          }
        });
    }
    else
    {
      alert("You must answer all the questions.");
    }
  }

  nextQuestion()
  {
    var index: number = this.questions.indexOf(this.currentQuestion); //Get the current index

    if(index != this.questions.length - 1) //If the index is not last in the array
    {
      this.currentQuestion = this.questions[index + 1];
      this.currentQuestionIndex++;
    }
    else //If the index is last in the array, show the first question
    {
      this.currentQuestion = this.questions[0];
      this.currentQuestionIndex = 1;
    }

    this.ratingBar.changeRating(this.currentQuestion.rating); //Tell the rating bar to update the rating in the UI
  }

  previousQuestion()
  {
    var index: number = this.questions.indexOf(this.currentQuestion); //Get the current index

    if(index != 0) //If the index is not first in the array 
    {
      this.currentQuestion = this.questions[index - 1];
      this.currentQuestionIndex--;
    }
    else //If the index is first in the array, show the last question
    {
      this.currentQuestion = this.questions[this.questions.length - 1];
      this.currentQuestionIndex = this.questions.length;
    }

    this.ratingBar.changeRating(this.currentQuestion.rating); //Tell the rating bar to update the rating in the UI
  }

  private assignQuestions(objectArray): Question[] //Arrange the data received
  {
    var questions: Question[] = [];

    for(var i in objectArray) //For each question create an object
    {
      var question: Question = 
      {
        id: objectArray[i][0],
        categoryCode: objectArray[i][1],
        name: objectArray[i][2],
        content: objectArray[i][3],
        rating: objectArray[i][4]
      }

      if(question.rating > 0) //If the question is rated
      {
        this.questionsRated++;
      }

      questions.push(question);
    }

    return questions;
  }
}
