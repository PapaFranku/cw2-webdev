import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationExtras } from "@angular/router";

import { UniService } from '../../services/uni.service';
import { Module } from "../module/IModule";
import { Question } from "../module/IQuestion";

@Component({
  selector: 'student-home',
  templateUrl: './student-home.component.html',
  styleUrls: ['./student-home.component.css']
})

export class StudentHomeComponent implements OnInit //Reperesents the container for the input page
{
  studentModules: Module[]; //Holds the modules for the current student
  studentMatric: string; //Holds the matric number for the current student
  studentNames: string; //Holds the names of the current student

  constructor(private uniService: UniService,
              private router: Router,
              private activatedRoute: ActivatedRoute)
  { }

  ngOnInit() //Called when the component is instantiated
  {
    this.activatedRoute.queryParams.subscribe((params: Params) => 
    {
         this.studentMatric = params["u"];

         if(!this.studentMatric)
         {
           return;
         }

         this.uniService.getModulesForStudent(this.studentMatric) //Get the modules for the matriculation number provided
            .subscribe(res => 
            { 
              if(res.length != 0)
              {
                this.studentNames = res[0][0] + " " + res[0][1];
                this.studentModules = this.assignModules(res);
              }
              else
              {
                this.studentMatric = "invalid";
              }
            });
    });
  }

  private assignModules(objectArray): Module[] //Arrange the data received
  {
    var modules: Module[] = [];

    for(var i = 1; i < objectArray.length; i++)
    {
      var module: Module = 
      {
        moduleCode: objectArray[i][0],
        moduleName: objectArray[i][1]
      }
      
      modules.push(module);
    }

    return modules;
  }

  navigateToStudentPage(matricNumber: string) //Called when the submit button was pressed if no matric number
  {                                             //was provided in the url
    var navExtras: NavigationExtras = 
    {
      queryParams: { "u": matricNumber }
    }

    this.router.navigate([""], navExtras);
  }
}
