import { Component, Input } from '@angular/core';

import { UniService } from "../../services/uni.service";
import { DataRow } from "./IDataRow";

@Component({
  selector: 'scores-table',
  templateUrl: './scores-table.component.html',
  styleUrls: ['./scores-table.component.css']
})
export class ScoresTableComponent //Represents a single table, which holds moduel data
{
  @Input() data: DataRow[]; //Recieves its information from the parent component
}
