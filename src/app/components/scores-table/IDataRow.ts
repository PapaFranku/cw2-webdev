export interface DataRow
{
    moduleCode: string,
    section: string,
    questionNumber: string,
    question: string,
    agreeRate: string
}